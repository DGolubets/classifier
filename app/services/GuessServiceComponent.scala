package services

import java.util.UUID

import data.model._
import services.model.Guess

import scala.concurrent.Future

/**
 * Business logic will go here...
 */
trait GuessServiceComponent {

  val guessService: GuessService

  trait GuessService {

    /**
     * Searches for facilities which process a commodity.
     * @param id commodity id
     *           can be an id of any commodity category
     * @return List of guesses
     */
    def facilitiesProcessingCommodity(id: UUID): Future[List[Guess]]
  }
}

