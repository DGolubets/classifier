package services

import java.util.UUID

import data.model._

import scala.concurrent.Future

/**
 * Business logic will go here...
 */
trait ClassifierServiceComponent {

  val classifierService: ClassifierService

  trait ClassifierService {

    /**
     * Creates a category
     * @param category category to create
     * @return
     */
    def createCategory(category: Category): Future[Unit]

    /**
     * Updates existing category
     * @param category category to update
     * @return
     */
    def updateCategory(category: Category): Future[Boolean]

    /**
     * Removes existing category by id
     * @param id category id
     * @return
     */
    def removeCategory(id: UUID): Future[Boolean]

    /**
     * Finds a category by id
     * @param id category id
     * @return
     */
    def findCategoryById(id: UUID): Future[Option[Category]]

    /**
     * Finds all categories
     * @return
     */
    def findAllCategories(): Future[List[Category]]
  }
}
