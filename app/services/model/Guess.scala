package services.model

import java.util.UUID

/**
 * Describes guess result.
 * @param resultId id of a corresponding entity (e.g. facility)
 * @param likelihood 0 -> 1
 */
case class Guess(resultId: UUID, likelihood: Float)