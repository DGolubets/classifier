package services.impl

import java.util.UUID

import data.impl.mongo.MongoClassifierRepositoryComponent
import data.model.CategoryTag
import data.model.CategoryTag.CategoryTag
import data.model.{Category, CategoryTag}
import services.GuessServiceComponent
import services.model.Guess

import scala.concurrent.Future

/**
 * Created by Dima on 12.09.2015.
 */
trait DefaultGuessServiceComponent extends GuessServiceComponent {
  this: MongoClassifierRepositoryComponent =>

  override lazy val guessService = new DefaultGuessService

  class DefaultGuessService extends GuessService {

    import scala.concurrent.ExecutionContext.Implicits.global
    import scala.async.Async.{async, await}

    /**
     * Searches for facilities which process a commodity.
     * @param id commodity id
     *           can be an id of any commodity category
     * @return List of guesses
     */
    override def facilitiesProcessingCommodity(id: UUID): Future[List[Guess]] = {
      def findExactFacilities(category: Category): Future[List[Category]] = {
        category match {
          case c if c.externalId.nonEmpty => Future.successful(List(c))
          case c if c.externalId.isEmpty => classifierRepository.findChildren(c.id).map(_.filter(_.externalId.nonEmpty))
        }
      }

      findConnectedCategories(id, CategoryTag.Facility).flatMap { categories =>
        Future.sequence {
          categories.map {
            case (c, l) => findExactFacilities(c).map(_.map(c1 => c1 -> l))
          }
        }.map(_.flatten.map{case (c, l) => Guess(c.id, l)})
      }
    }

    /**
     * Returns list of pairs: category -> likelihood
     * @param id category id
     * @param tag type of related categories
     * @return
     */
    private def findConnectedCategories(id: UUID, tag: CategoryTag): Future[List[(Category, Float)]] = {

      /**
       * Gets the whole category tree which holds the specified category.
       * @param id
       * @return
       */
      def getCategoryTree(id: UUID): Future[List[Category]] = {
        classifierRepository.findByIdWithAncestors(id).flatMap { list1 =>
          val root = list1.find(_.parentId.isEmpty).get
          classifierRepository.findByIdWithDescedants(root.id)
        }
      }

      /**
       * Gets a list of likelihoods for related subset of category tree.
       * @param id
       * @return
       */
      def getCategoriesLikelihood(id: UUID): Future[List[(Category, Float)]] = {
        getCategoryTree(id).map { categories =>
          val mapById = categories.map(c => c.id -> c).toMap
          val groupByParentId = categories.filter(_.parentId.nonEmpty).groupBy(c => c.parentId.get).withDefaultValue(Nil)

          def countSubCategories(id: UUID) = groupByParentId(id).count(_.externalId.isEmpty) // exclude external nodes

          def asceding(base: Float, id: Option[UUID]): List[(Category, Float)] = {
            id match {
              case Some(uuid) =>
                val cat = mapById(uuid)
                val likelihood = base / (countSubCategories(uuid) max 1)
                (cat, likelihood) :: asceding(likelihood, cat.parentId)
              case _ => Nil
            }
          }

          def desceding(prev: List[(Category, Float)]): List[(Category, Float)] = {
            val children = prev.flatMap {
              case (cat, l) => groupByParentId(cat.id).map(child => (child, l))
            }
            children match {
              case list if list.nonEmpty => prev ++ desceding(list)
              case list if list.isEmpty => prev
              case _ => Nil
            }
          }

          val current = mapById(id)
          asceding(1f, current.parentId) ::: desceding(List((current, 1f)))
        }
      }

      /**
       * Gets all merged connections likelihood from related subset of category tree.
       * @return
       */
      def getConnectionsLikelihood(): Future[List[(UUID, Float)]] = {
        getCategoriesLikelihood(id).map { categories =>
          mergeLikelihoods(categories.flatMap { case (c, l) => c.relations.map(r => r -> l) })
        }
      }

      /**
       * Sums up all likelihoods of the same entities
       * @param source
       * @tparam T
       * @return
       */
      def mergeLikelihoods[T](source: List[(T, Float)]): List[(T, Float)] = {
        source
          .groupBy { case (key, l) => key }
          .map { case (key, list) => key -> (list.map(_._2).sum min 1) }
          .toList
      }

      getConnectionsLikelihood().flatMap { connections =>
        val likelihoodMap = connections.toMap
        val results: Future[List[(Category, Float)]] = classifierRepository.findByIdsAndTag(connections.map(_._1), tag).flatMap { connectedCategories =>
          val cons = connectedCategories.map(c => c.id -> likelihoodMap(c.id))
          Future.sequence {
            cons.map {
              case (con, l) => getCategoriesLikelihood(con).map(_.map { case (c, l1) => (c, l * l1) })
            }
          }.map(_.flatten).map(r => mergeLikelihoods(r))
        }
        results
      }
    }
  }
}
