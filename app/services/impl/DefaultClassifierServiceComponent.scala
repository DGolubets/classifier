package services.impl

import java.util.UUID

import data.ClassifierRepositoryComponent
import data.model.Category
import services._

import scala.concurrent.Future

/**
 * Default service implementation.
 */
trait DefaultClassifierServiceComponent extends ClassifierServiceComponent {
  this: ClassifierRepositoryComponent =>

  override lazy val classifierService: ClassifierService = new DefaultClassifierService

  class DefaultClassifierService extends ClassifierService {
    /**
     * Creates a category
     * @param category category to create
     * @return
     */
    override def createCategory(category: Category): Future[Unit] =
      classifierRepository.create(category)

    /**
     * Updates existing category
     * @param category category to update
     * @return
     */
    override def updateCategory(category: Category): Future[Boolean] =
      classifierRepository.update(category)

    /**
     * Removes existing category by id
     * @param id category id
     * @return
     */
    override def removeCategory(id: UUID): Future[Boolean] =
      classifierRepository.delete(id)

    /**
     * Finds a category by id
     * @param id category id
     * @return
     */
    override def findCategoryById(id: UUID): Future[Option[Category]] =
      classifierRepository.findById(id)

    /**
     * Finds all categories
     * @return
     */
    override def findAllCategories(): Future[List[data.model.Category]] =
      classifierRepository.findAll()
  }

}
