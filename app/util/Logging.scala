package util

import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory


/**
 * Logging mixin.
 */
trait Logging {

  protected lazy val log = Logger(LoggerFactory.getLogger(this.getClass))

}
