package util

import com.typesafe.config.{Config, ConfigFactory}

/**
 * Configuration mixin.
 */
trait Configuration {
  protected def config: Config = Configuration.global
}

/**
 * Singleton configuration. Use when DI can't be applied.
 */
object Configuration {
  lazy val global = ConfigFactory.load()
}
