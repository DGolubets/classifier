package util

import java.nio.ByteBuffer
import java.util.UUID

import scala.util.Try

/**
 * Useful type conversions.
 */
object Converter {
  def bytesToUUID(bytes: Array[Byte]): Try[UUID] = Try {
    val buffer = ByteBuffer.wrap(bytes)
    new UUID(buffer.getLong, buffer.getLong)
  }

  def uuidToBytes(uuid: UUID): Array[Byte] = {
    val buffer = ByteBuffer.wrap(new Array[Byte](16))
    buffer.putLong(uuid.getMostSignificantBits)
    buffer.putLong(uuid.getLeastSignificantBits)
    buffer.array()
  }

  // parse uuid without dashes
  def strToUUID(s:String):Try[UUID] = Try {
    val builder = new StringBuilder(36)
    builder.append(s.replace("-", ""))
    builder.insert(8, "-")
    builder.insert(13, "-")
    builder.insert(18, "-")
    builder.insert(23, "-")

    UUID.fromString(builder.toString())
  }
}
