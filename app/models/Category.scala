package models

import java.util.UUID

import data.model.CategoryTag.CategoryTag

/**
 * Category node view model
 */
case class Category(id: Option[UUID],
                    parentId: Option[UUID],
                    relations: Option[List[UUID]],
                    externalId: Option[String],
                    tag: CategoryTag,
                    name: String,
                    description: Option[String])
