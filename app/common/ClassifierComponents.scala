package common

import controllers.{GuessApiController, DemoController, ClassifierApiController}
import data.impl.mongo.MongoClassifierRepositoryComponent
import play.api.ApplicationLoader.Context
import play.api.BuiltInComponentsFromContext
import play.api.routing.Router
import services.impl.{DefaultGuessServiceComponent, DefaultClassifierServiceComponent}
import router.Routes

/**
 * Created by Dima on 30.06.2015.
 */
class ClassifierComponents(context: Context) extends BuiltInComponentsFromContext(context) {
  lazy val router: Router = new Routes(httpErrorHandler, cake, cake, cake, assets)
  lazy val assets = new controllers.Assets(httpErrorHandler)
  lazy val cake = new DemoController with ClassifierApiController with GuessApiController
    with DefaultClassifierServiceComponent with DefaultGuessServiceComponent
    with MongoClassifierRepositoryComponent
}
