package controllers.util

import play.api.libs.concurrent.Execution.Implicits._
import play.api.mvc.{ActionBuilder, Request, Result}

import scala.concurrent.Future

/**
 * Action for REST API methods.
 * Adds no-cache header.
 */
object ApiAction extends ActionBuilder[Request] {
  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] =
    block(request).map(_.withHeaders(("Cache-Control", "no-cache")))
}
