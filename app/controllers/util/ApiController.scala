package controllers.util

import play.api.libs.json.Json
import play.api.libs.json.Json.JsValueWrapper
import play.api.mvc.Controller

/**
 * Base controller for REST API
 */
trait ApiController extends Controller {
  implicit val executionContext = play.api.libs.concurrent.Execution.Implicits.defaultContext
}
