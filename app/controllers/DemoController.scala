package controllers

import play.api.mvc.{Action, Controller}


trait DemoController extends Controller {
  def index = Action {
    Ok(views.html.demo())
  }
}
