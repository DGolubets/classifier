package controllers

import java.util.UUID

import _root_.util.Converter
import data.model.CategoryTag
import data.model.CategoryTag
import data.model.CategoryTag.CategoryTag
import models.Category
import play.api.mvc.{BodyParsers, Controller}
import controllers.util.{ApiController, ApiAction}
import play.api.libs.json._
import services.ClassifierServiceComponent

import scala.concurrent.Future
import scala.util.{Try, Success}

/**
 * Classifier REST API
 */
trait ClassifierApiController extends ApiController {
  this: ClassifierServiceComponent =>

  implicit object categoryTagFormat extends Format[CategoryTag] {
    override def reads(json: JsValue): JsResult[CategoryTag] =
      Try(CategoryTag.withName(json.as[String])) match {
        case Success(tag) => JsSuccess(tag)
        case _ => JsError("Invalid tag")
      }

    override def writes(o: CategoryTag): JsValue = JsString(o.toString)
  }

  implicit val categoryReads = Json.reads[Category]
  implicit val categoryWrites = Json.writes[Category]

  def save = ApiAction.async(BodyParsers.parse.json) { request =>
    request.body.validate[Category].fold(
      errors => Future.successful(BadRequest(JsError.toJson(errors))),
      model => if (model.id.isEmpty) {
        val id = UUID.randomUUID()
        classifierService.createCategory(model.copy(id = Some(id))).map(_ => Ok(Json.obj("message" -> "Category has been created.", "id" -> id)))
      } else {
        classifierService.updateCategory(model).map { updated =>
          if (updated) Ok(Json.obj("message" -> "Category has been updated.")) else NotFound
        }
      }
    )
  }

  def remove(categoryId: String) = ApiAction.async {
    Converter.strToUUID(categoryId) match {
      case Success(uuid) => classifierService.removeCategory(uuid).map(removed => if(removed) Ok else NotFound)
      case _ => Future.successful(NotFound)
    }
  }

  def findById(categoryId: String) = ApiAction.async {
    Converter.strToUUID(categoryId) match {
      case Success(uuid) => classifierService.findCategoryById(uuid).map {
        case Some(category) => Ok(Json.toJson(toViewModel(category)))
        case _ => NotFound
      }
      case _ => Future.successful(NotFound)
    }
  }

  def findAll() = ApiAction.async {
    classifierService.findAllCategories().map { list => Ok(Json.toJson(list.map(c => toViewModel(c)))) }
  }

  // mappers
  import scala.language.implicitConversions
  implicit def fromViewModel(category: Category): data.model.Category =
    data.model.Category(category.id.get, category.parentId, category.relations.getOrElse(Nil), category.externalId, category.tag, category.name, category.description)
  implicit def toViewModel(category: data.model.Category): Category =
    Category(Some(category.id), category.parentId, Some(category.relations), category.externalId, category.tag, category.name, category.description)
}
