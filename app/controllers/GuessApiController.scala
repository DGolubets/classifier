package controllers

import _root_.util.Converter
import controllers.util.{ApiController, ApiAction}
import play.api.libs.json._
import play.api.mvc.Controller
import services.GuessServiceComponent
import services.model.Guess

import scala.concurrent.Future
import scala.util.Success

/**
 * Classifier REST API
 */
trait GuessApiController extends ApiController {
  this: GuessServiceComponent =>

  implicit val guessWriter = Json.writes[Guess]

  def facilitiesProcessingCommodity(id: String) = ApiAction.async {
    Converter.strToUUID(id) match {
      case Success(uuid) => guessService.facilitiesProcessingCommodity(uuid).map(results => Ok(Json.toJson(results)))
      case _ => Future.successful(NotFound)
    }
  }
}
