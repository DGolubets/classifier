package data

import java.util.UUID

import data.model.CategoryTag.CategoryTag
import data.model._

import scala.concurrent.Future

/**
 * Classifier repository component.
 */
trait ClassifierRepositoryComponent {

  val classifierRepository: ClassifierRepository

  trait ClassifierRepository {

    /**
     * Creates a category
     * @param category to create
     * @return
     */
    def create(category: Category): Future[Unit]

    /**
     * Updates existing category
     * @param category to update
     * @return
     */
    def update(category: Category): Future[Boolean]

    /**
     * Removes existing category by id
     * @param id of the category
     * @return
     */
    def delete(id: UUID): Future[Boolean]

    /**
     * Returns a category by id
     * @return
     */
    def findById(id: UUID): Future[Option[Category]]

    /**
     * Searches for a category with all subcategories
     * @param id
     * @return
     */
    def findByIdWithDescedants(id: UUID): Future[List[Category]]

    /**
     * Searches for a category with all base categories.
     * @param id category id
     * @return
     */
    def findByIdWithAncestors(id: UUID): Future[List[Category]]

    /**
     * Searches for a category base categories.
     * @param id category id
     * @return
     */
    def findAncestors(id: UUID): Future[List[Category]]

    /**
     * Searches for a category subcategories
     * @param id category id
     * @return
     */
    def findDescedants(id: UUID): Future[List[Category]]

    /**
     * Searches for a category direct subcategories
     * @param id category id
     * @return
     */
    def findChildren(id: UUID): Future[List[Category]]


    /**
     * Searches for a category with id in range and specific tag
     * @param ids category ids seq
     * @param tag category tag
     * @return
     */
    def findByIdsAndTag(ids: Seq[UUID], tag: CategoryTag): Future[List[Category]]

    /**
     * Returns all categories
     * @return
     */
    def findAll(): Future[List[Category]]

    /**
     * Counts child categories.
     * @param id category id
     * @return
     */
    def getChildrenCount(id: UUID): Future[Int]
  }
}
