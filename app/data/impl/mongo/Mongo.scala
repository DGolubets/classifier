package data.impl.mongo

import reactivemongo.api.{MongoConnection, MongoDriver}
import util.Configuration

/**
 * Mongo manager.
 */
private object Mongo extends Configuration {

  // parse config
  private lazy val configSection = config.getConfig("com.commodityvectors.classifier.mongodb")

  // get an instance of the driver
  // (creates an actor system as said in the docs)
  private val driver = new MongoDriver

  // gets a connection (creates a connection pool)
  // let it crash if config value is invalid or missing - it's critical error
  private val connection = driver.connection(MongoConnection.parseURI(configSection.getString("uri")).get)

  /**
   * An execution context for Mongo.
   * Let's use scala default for now.
   */
  implicit val executionContext = scala.concurrent.ExecutionContext.Implicits.global

  /**
   * Gets a reference to the database
   * @param name of the database
   * @return
   */
  def db(name: String) = connection(name)

  /**
   * Posts database
   */
  lazy val defaultDatabase = db(configSection.getString("database"))

}
