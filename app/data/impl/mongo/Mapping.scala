package data.impl.mongo

import java.util.UUID

import data.model._
import reactivemongo.api.commands.WriteResult
import reactivemongo.bson._
import util.Converter

import scala.concurrent.Future

/**
 * Implicit conversions for MongoDB
 */
private[data] object Mapping {

  /**
   * UUID mapper.
   */
  implicit object UUIDHandler extends BSONHandler[BSONBinary, UUID] {
    def read(bson: BSONBinary): UUID = Converter.bytesToUUID(bson.value.readArray(16)).get

    def write(id: UUID): BSONBinary = BSONBinary(Converter.uuidToBytes(id), Subtype.UuidSubtype)
  }

  /**
   * Category mapper.
   */
  implicit object CategoryMapper extends BSONDocumentReader[Category] with BSONDocumentWriter[Category] {
    def read(doc: BSONDocument): Category =
      Category(
        doc.getAs[UUID]("_id").get,
        doc.getAs[UUID]("parentId"),
        doc.getAs[List[UUID]]("relations").get,
        doc.getAs[String]("externalId"),
        doc.getAs[String]("tag").map(tagName => CategoryTag.withName(tagName)).get,
        doc.getAs[String]("name").get,
        doc.getAs[String]("description"))

    def write(category: Category): BSONDocument =
      BSONDocument(
        "_id" -> category.id,
        "parentId" -> category.parentId,
        "relations" -> category.relations,
        "externalId" -> category.externalId,
        "tag" -> category.tag.toString,
        "name" -> category.name,
        "description" -> category.description)
  }


  /**
   * Helps dealing with MongoDB results.
   */
  implicit class MongoResultMapper(result: Future[WriteResult]) {

    import Mongo.executionContext

    /**
     * Maps results with WriteResult indicating error to failure.
     */
    def noError(): Future[WriteResult] = result flatMap {
      case e if e.inError => Future.failed(new Exception(e.message))
      case _ => result
    }
  }
}

