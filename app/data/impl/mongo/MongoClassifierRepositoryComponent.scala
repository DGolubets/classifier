package data.impl.mongo

import java.util.UUID

import data.ClassifierRepositoryComponent
import data.model.CategoryTag.CategoryTag
import data.model._
import reactivemongo.bson.BSONDocument
import scala.concurrent.Future


trait MongoClassifierRepositoryComponent extends ClassifierRepositoryComponent {

  override lazy val classifierRepository: ClassifierRepository = new MongoClassifierRepository

  class MongoClassifierRepository extends ClassifierRepository {

    import Mongo.executionContext
    import Scheme._
    import Mapping._
    import reactivemongo.api.commands.bson.BSONCountCommand.{ Count, CountResult }
    import reactivemongo.api.commands.bson.BSONCountCommandImplicits._

    /**
     * Creates a category
     * @param category to create
     * @return
     */
    override def create(category: Category): Future[Unit] =
      categoriesCollection.insert(category).noError.map(_ => ())

    /**
     * Updates existing category
     * @param category to update
     * @return
     */
    override def update(category: Category): Future[Boolean] =
      categoriesCollection.update(BSONDocument("_id" -> category.id), category).noError.map(_.n > 0)

    /**
     * Removes existing category by id
     * @param id of the category
     * @return
     */
    override def delete(id: UUID): Future[Boolean] = {
      val deleteOperation = categoriesCollection.remove(BSONDocument("_id" -> id)).noError.map(_.n > 0)

      deleteOperation.flatMap { success =>
        if(success) {
          // clear parent references to deleting record
          val parentCleanup = categoriesCollection.find(BSONDocument("parentId" -> id)).cursor[Category]().collect[List]().flatMap { categories =>
            Future.sequence(categories.map(c => update(c.copy(parentId = None))))
          }

          // clear relations references to deleting record
          val relationsCleanup = categoriesCollection.find(BSONDocument("relations" -> BSONDocument("$all" -> Array(id)))).cursor[Category]().collect[List]().flatMap { categories =>
            Future.sequence(categories.map(c => update(c.copy(relations = c.relations.filter(r => r != id)))))
          }

          Future.sequence(Seq(parentCleanup, relationsCleanup)).map(_ => true)
        }
        else Future.successful(false)
      }
    }

    /**
     * Returns a category by id
     * @return
     */
    override def findById(id: UUID): Future[Option[Category]] =
      categoriesCollection.find(BSONDocument("_id" -> id)).cursor[Category]().headOption

    /**
     * Returns all commodity categories
     * @return
     */
    override def findAll(): Future[List[Category]] =
      categoriesCollection.find(BSONDocument()).cursor[Category]().collect[List]()

    /**
     * Searches for a category with all subcategories
     * @param id
     * @return
     */
    override def findByIdWithDescedants(id: UUID): Future[List[Category]] = {
      for {
        self <- findById(id)
        descedants <- findDescedants(id)
      } yield self match {
        case Some(category) => category :: descedants
        case _ => descedants
      }
    }

    /**
     * Searches for a category with all base categories
     * @param id category id
     * @return
     */
    override def findByIdWithAncestors(id: UUID): Future[List[Category]] = {
      def search(id: UUID): Future[List[Category]] = findById(id).flatMap {
        case Some(category) if category.parentId.nonEmpty => search(category.parentId.get).map(category :: _)
        case Some(category) if category.parentId.isEmpty => Future.successful(List(category))
        case _ => Future.successful(Nil)
      }

      search(id)
    }

    /**
     * Searches for a category base categories.
     * @param id category id
     * @return
     */
    override def findAncestors(id: UUID): Future[List[Category]] =
      findByIdWithAncestors(id).map(_.filter(_.id != id))

    /**
     * Searches for a category subcategories
     * @param id category id
     * @return
     */
    override def findDescedants(id: UUID): Future[List[Category]] = {
      def search(query: BSONDocument): Future[List[Category]] =
        categoriesCollection.find(query).cursor[Category]().collect[List]().flatMap { categories =>
          if(categories.nonEmpty)
            search(childrenQuery(categories.map(_.id))).map(children => categories ++ children)
          else Future.successful(categories)
        }

      def childrenQuery(ids: List[UUID]): BSONDocument =
        BSONDocument("parentId" -> BSONDocument("$in" -> ids))

      search(childrenQuery(List(id)))
    }

    /**
     * Searches for a category direct subcategories
     * @param id category id
     * @return
     */
    override def findChildren(id: UUID): Future[List[Category]] = {
      val query = BSONDocument("parentId" ->id)

      categoriesCollection.find(query).cursor[Category]().collect[List]()
    }

    /**
     * Searches for a category with id in range and specific tag
     * @param ids category ids seq
     * @param tag category tag
     * @return
     */
    override def findByIdsAndTag(ids: Seq[UUID], tag: CategoryTag): Future[List[Category]] = {
      val query = BSONDocument("$and" -> Seq(
        BSONDocument("_id" -> BSONDocument("$in" -> ids)),
        BSONDocument("tag" -> tag.toString)))

      categoriesCollection.find(query).cursor[Category]().collect[List]()
    }

    /**
     * Counts child categories.
     * @param id category id
     * @return
     */
    override def getChildrenCount(id: UUID): Future[Int] = {
      val query = BSONDocument("parentId" -> id)
      val command = Count(query)
      categoriesCollection.runCommand(command).map(_.value)
    }
  }
}
