package data.impl.mongo

import reactivemongo.api.collections.bson.BSONCollection
import reactivemongo.api.indexes.{IndexType, Index}

/**
 * Keeps references to collections and ensures indexes.
 */
object Scheme {
  import Mongo.executionContext

  val categoriesCollection = Mongo.defaultDatabase.collection[BSONCollection]("categories")

  // indecies
  categoriesCollection.indexesManager.ensure(Index(key = Seq("externalId" -> IndexType.Ascending), sparse = true))
  categoriesCollection.indexesManager.ensure(Index(key = Seq("tag" -> IndexType.Ascending, "name" -> IndexType.Ascending) , unique = true))
}
