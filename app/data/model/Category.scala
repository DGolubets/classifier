package data.model

import java.util.UUID

/**
 * Category node.
 * @param id identifier
 * @param parentId base category id
 * @param relations related categories
 * @param externalId id of external entity, represented by this category node
 * @param tag describes category nature
 * @param name category name
 * @param description category description
 */
case class Category(id: UUID,
                    parentId: Option[UUID],
                    relations: List[UUID],
                    externalId: Option[String],
                    tag: CategoryTag.CategoryTag,
                    name: String,
                    description: Option[String])


object CategoryTag extends Enumeration {
  type CategoryTag = Value

  // explicitly set values, cos they are saved to db
  val Facility = Value(0, "facility")
  val Commodity = Value(1, "commodity")
  val Vessel = Value(2, "vessel")
  val Cargo = Value(3, "cargo")
}