var CategoryMultiSelector = React.createClass({
    getInitialState: function(){
        var initialState = {
            showAdd: true,
            valueToAdd: null
        };

        var selectedCategoriesIds = this.props.value || [];
        var availableCategory = this.props.categories.find(function(c){
            return selectedCategoriesIds.indexOf(c.id) < 0;
        });
        if(availableCategory){
            initialState.valueToAdd = availableCategory.id;
        }

        return initialState;
    },
    componentWillReceiveProps: function(nextProps){
        var nextState = {
            valueToAdd: null
        };

        var selectedCategoriesIds = nextProps.value || [];
        var availableCategory = nextProps.categories.find(function(c){
            return selectedCategoriesIds.indexOf(c.id) < 0;
        });
        if(availableCategory){
            nextState.valueToAdd = availableCategory.id;
        }

        this.setState(nextState);
    },
    render: function(){
        var self = this;
        var selectedCategoriesIds = this.props.value || [];
        var selectedCategories = this.props.categories
            .filter(function(c){
                return selectedCategoriesIds.indexOf(c.id) >= 0;
            })
            .sort(function(a, b){
                return a.name.localeCompare(b.name);
            })
            .map(function(c){
                return (
                    <div key={c.id} className="multiSelector-category">
                        <button className="btn btn-sm btn-default btn-danger" type="button" onClick={ self.handleRemove.bind(self, c) }>-</button>
                        <div className="multiSelector-category-name">{c.name}</div>
                    </div>
                );
            });

        var availableCategories = this.props.categories
            .filter(function(c){
                return selectedCategoriesIds.indexOf(c.id) < 0;
            })
            .sort(function(a, b){
                return a.name.localeCompare(b.name);
            });

        var showAdd = this.state.showAdd && availableCategories.length > 0;

        return (
            <div className="multiSelector">
                { showAdd ? (
                    <div className="multiSelector-header">
                        <button className="btn btn-sm btn-default btn-primary" type="button" onClick={ this.handleAdd } >+</button>
                        <CategorySelector categories={availableCategories} value={this.state.valueToAdd} onChange={this.handleAddChange} />
                    </div>
                ) : undefined }
                <div className="multiSelector-categories">{selectedCategories}</div>
            </div>
        );
    },
    handleAddChange: function(value){
        this.setState({ valueToAdd: value });
    },
    handleAdd: function(){
        var newValues = Immutable.List(this.props.value)
            .push(this.state.valueToAdd)
            .toArray();
        this.notifyChange(newValues);
    },
    handleRemove: function(category){
        var newValues = Immutable.List(this.props.value);
        var delIndex = newValues.indexOf(category.id);
        newValues = newValues.delete(delIndex).toArray();
        this.notifyChange(newValues);
    },
    handleCancel: function(){

    },
    notifyChange: function(value){
        this.props.onChange(value);
    }
});