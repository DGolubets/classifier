var Toolbar = React.createClass({
    handleCreate: function(category){
        this.props.onCreate(category);
        this.navigateHome();
    },
    handleUpdate: function(category){
        this.props.onUpdate(category);
        this.navigateHome();
    },
    handleRemove: function(){
        if(confirm("Are you sure?")) {
            this.props.onRemove(this.props.selectedCategory);
        }
    },
    navigateHome: function(category){
        this.navigatePage("home");
    },
    navigatePage: function(page){
        this.setState({
            page: page
        });
    },
    getInitialState: function () {
        return {
            page: "home"
        }
    },
    componentWillReceiveProps: function(nextProps){
        if(!nextProps.selectedCategory && this.state.page == "update"){
            this.setState({
                page: "home"
            });
        }
    },
    render: function(){
        var content;
        var title;
        switch(this.state.page){
            case "home":
                title = "Actions";
                content = [
                    (<button key={1} className="btn btn-lg btn-primary toolbar-button" onClick={this.navigatePage.bind(this, "create")}>Create</button>),
                    (<button key={2} className="btn btn-lg btn-primary toolbar-button" onClick={this.navigatePage.bind(this, "update")} disabled={!this.props.selectedCategory}>Update</button>),
                    (<button key={3} className="btn btn-lg btn-danger toolbar-button" onClick={this.handleRemove} disabled={!this.props.selectedCategory}>Remove</button>)
                ];
                break;
            case "update":
                title = "Update";
                content = <CategoryForm title="Update"
                                        categories={this.props.categories}
                                        category={this.props.selectedCategory}
                                        onSave={this.handleUpdate} onCancel={this.navigateHome} />;
                break;
            case "create":
                title = "Create";
                content = <CategoryForm title="Create"
                                        categories={this.props.categories}
                                        onSave={this.handleCreate} onCancel={this.navigateHome} />;
                break;
        }

        return (
            <div className="toolbar">
                <h2>{title}</h2>
                {content}
            </div>
        );
    }
});