var CategoryForm = React.createClass({
    handleSave: function(){
        this.props.onSave(this.state.fields);
    },
    handleCancel: function(){
        this.props.onCancel();
    },
    handleChange: function(e){
        this.changeField(e.target.name, e.target.value);
    },
    handleSelect: function(e) {
        // if value is empty string set it to null to reset it
        this.changeField(e.target.name, e.target.value || null);
    },
    handleMultiSelect: function(e) {
        var options = e.target.options;
        var values = [];
        for (var i = 0; i < options.length; i++) {
            if (options[i].selected) {
                values.push(options[i].value);
            }
        }
        this.changeField(e.target.name, values);
    },
    changeField: function(fieldName, fieldValue){
        var fields = {};
        fields[fieldName] = fieldValue;
        fields = React.addons.update(this.state.fields, {$merge: fields });
        this.setState({
            fields: fields
        });
    },
    isDirty: function(){
        var propsFields = Immutable.fromJS(this.props.category);
        var stateFields = Immutable.fromJS(this.state.fields);

        return !Immutable.is(propsFields, stateFields);
    },
    getInitialState: function (props){
        props = props || this.props;
        return {
            fields: props.category || {
                // default fields for new category
                tag: CategoryTags.facility
            }
        };
    },
    componentWillReceiveProps: function(nextProps){
        // probably another category has been selected
        // so we need to reset fields
        this.setState(this.getInitialState(nextProps));
    },
    render: function(){
        // prepare the data

        var categories = this.props.categories;
        var category = this.state.fields;
        var fields = this.state.fields;

        var validParents = categories.filter(function(c){
            return c.id != category.id; //&& c.tag == category.tag;
        });

        // an option to have no parent
        validParents.unshift({ id: null, name: "None"});

        var validRelations =  categories.filter(function(c){
            // let the relations to be set with any other category type
            return c.id != category.id && c.tag != category.tag;
        });

        // start rendering

        var relationOptions = validRelations.map(function(c){
            return <option key={c.id} value={c.id}>{c.name}</option>;
        });

        var tagOptions = [
            { key: CategoryTags.facility, name: "Facility" },
            { key: CategoryTags.vessel, name: "Vessel" },
            { key: CategoryTags.cargo, name: "Cargo" },
            { key: CategoryTags.commodity, name: "Commodity" }
        ].map(function(tag){
            return <option key={tag.key} value={tag.key}>{tag.name}</option>;
        });

        var isNew = !fields.id;
        var dirty = this.isDirty();
        var canSave = dirty && fields.tag && fields.name;

        return (
            <form className="category-form">
                {
                    !isNew ? (
                        <div className="form-group">
                            <label>Id</label>
                            <input name="id" className="form-control" type="text" readOnly="true" disabled="true" value={fields.id}  />
                        </div>
                    ) : undefined
                }
                {
                    isNew ? (
                        <div className="form-group">
                            <label>Tag</label>
                            <select name="tag" className="form-control" value={fields.tag} onChange={this.handleChange}>{tagOptions}</select>
                        </div>
                    ) : undefined
                }
                <div className="form-group">
                    <label>Parent</label>
                    <CategorySelector categories={validParents} value={fields.parentId} onChange={this.changeField.bind(this, 'parentId')} />
                </div>
                <div className="form-group">
                    <label>Relations</label>
                    <CategoryMultiSelector categories={validRelations} value={fields.relations} onChange={this.changeField.bind(this, 'relations')} />
                </div>
                <div className="form-group">
                    <label>External ID</label>
                    <input name="externalId" className="form-control" type="text" value={fields.externalId} onChange={this.handleChange} />
                </div>
                <div className="form-group">
                    <label>Name</label>
                    <input name="name" className="form-control" type="text" value={fields.name} onChange={this.handleChange} />
                </div>
                <div className="form-group">
                    <label>Description</label>
                    <input name="description" className="form-control" type="text" value={fields.description} onChange={this.handleChange} />
                </div>
                <button className="btn btn-primary" type="button" onClick={ this.handleSave } disabled={!canSave}>Save</button>
                <button className="btn btn-default" type="button" onClick={ this.handleCancel }>Cancel</button>
            </form>
        );
    }
});