var App = React.createClass({
    render: function() {
        return (
            <div id="app">
                <div id="navigation">
                    <ReactRouter.Link activeClassName="active" to="/graph">Graph</ReactRouter.Link>
                    <ReactRouter.Link activeClassName="active" to="/guess">Guess</ReactRouter.Link>
                </div>
                <div id="content">
                    {this.props.children}
                </div>
            </div>
        );
    }
});