var CategorySelector = React.createClass({
    getInitialState: function(){
        var props = this.props;
        var initialTag = CategoryTags.facility;

        var selectedCategory = props.categories.find(function(c){
            return c.id == props.value;
        });

        if(selectedCategory && selectedCategory.tag) {
            initialTag = selectedCategory.tag;
        }
        else {
            var anyCategory = props.categories.find(function(c){
                return c.tag;
            });
            if(anyCategory) {
                initialTag = anyCategory.tag;
            }
        }

        return {
            tag: initialTag
        };
    },
    componentWillReceiveProps: function(nextProps){
        var nextState = {};

        var selectedCategory = nextProps.categories.find(function(c){
            return c.id == nextProps.value;
        });

        if(selectedCategory && selectedCategory.tag) {
            nextState.tag = selectedCategory.tag;
        }

        this.setState(nextState);
    },
    render: function(){
        var self = this;

        // wanna try Immutable.js here, but other components don't use it yet, so I have to convert to it explicitly
        var categories = Immutable.List(this.props.categories);

        var tags = categories
            .groupBy(function(c){
                return c.tag;
            })
            .filter(function(v, k){
                return k;
            })
            .map(function(value, key){
                var tag = key;
                return <div key={tag} className={"selector-tag " + tag + (tag == self.state.tag ? " active" : "")} onClick={self.handleTagChange.bind(self, tag)}></div>
            })
            .toArray();

        var tagOptions = [
            { key: CategoryTags.facility, name: "Facility" },
            { key: CategoryTags.vessel, name: "Vessel" },
            { key: CategoryTags.cargo, name: "Cargo" },
            { key: CategoryTags.commodity, name: "Commodity" }
        ];

        var categoryOptions = categories
            .filter(function(c){
                return !c.tag || c.tag == self.state.tag;
            })
            .sort(function(a, b){
                // move undefined tag on top
                var tagDiff = (a.tag && 1) - (b.tag && 1);

                if(tagDiff != 0)
                    return tagDiff;
                return a.name > b.name;
            })
            .map(function(c){
                return <option key={c.id} value={c.id || ""}>{c.name}</option>
            })
            .toArray();

        return (
            <div className="selector">
                <div>{tags}</div>
                <div>
                    <select ref="categories" value={this.props.value || ""} className="form-control" onChange={this.handleCategoryChange}>{categoryOptions}</select>
                </div>
            </div>
        );
    },
    handleTagChange: function(tag){
        if(this.state.tag != tag) {
            this.setState({
                tag: tag
            });

            var selectedCategory = this.props.categories.find(function (c) {
                return c.tag == tag;
            });
            if(selectedCategory)
                this.notifyChange(selectedCategory.id);
            else
                this.notifyChange(null);
        }
    },
    handleCategoryChange: function(e){
        this.notifyChange(e.target.value);
    },
    notifyChange: function(categoryId){
        categoryId = categoryId || null;
        if(this.props.value != categoryId) {
            this.props.onChange(categoryId);
        }
    }
});