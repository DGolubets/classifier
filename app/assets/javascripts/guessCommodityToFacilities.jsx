var GuessCommodityToFacilities = React.createClass({
    componentDidMount: function(){
        var self = this;
        $.ajax({
            url: "/api/categories",
            success: function(data){
                var commodities = data
                    .filter(function(c){
                        return c.tag == CategoryTags.commodity;
                    })
                    .sort(function(a, b){
                        return a.name.localeCompare(b.name);
                    });

                var facilities = data
                    .filter(function(c){
                        return c.tag == CategoryTags.facility;
                    });

                self.setState({
                    commodities: commodities,
                    facilities: facilities,
                    selectedCommodityId: commodities.length > 0 ? commodities[0].id : null
                })
            }
        });
    },
    getInitialState: function(){
        return {
            commodities: [],
            selectedCommodityId: null,
            facilities: [],
            results: null,
            processing: false
        }
    },
    render: function(){
        var self = this;
        var results;

        if(this.state.results) {
            results = this.state.results
                .map(function (guess) {
                    var facility = self.state.facilities.find(function (f) {
                        return f.id == guess.resultId;
                    });
                    return {facility: facility, likelihood: guess.likelihood};
                })
                .sort(function(a, b){
                    return b.likelihood - a.likelihood;
                })
                .map(function (guess, i) {
                    return (
                        <tr key={guess.facility.id} className="guess-result">
                            <td>{i + 1}</td>
                            <td className="guess-result-name">{guess.facility.name}</td>
                            <td className="guess-result-likelihood">{guess.likelihood}</td>
                        </tr>
                    );
                });
        }

        return (
            <div className="guess">
                <h2>Which facilities process selected commodity?</h2>
                <div>
                    <CategorySelector categories={this.state.commodities} value={this.state.selectedCommodityId} onChange={this.handleSelection} />
                </div>
                <div>
                    <button className="btn btn-primary" disabled={!this.state.selectedCommodityId} onClick={this.handleGuess} >Guess</button>
                </div>
                { results ? (
                    <table className="guess-results table table-striped">
                        <tbody>
                        <tr>
                            <th>#</th>
                            <th>Facility</th>
                            <th>Likelihood</th>
                        </tr>
                        {results}
                        </tbody>
                    </table>
                    ) : undefined
                }
            </div>
        );
    },
    handleSelection: function(categoryId){
        this.setState({
            selectedCommodityId: categoryId,
            results: null
        });
    },
    handleGuess: function(){
        var self = this;
        this.setState({
            results: null,
            processing: true
        });
        $.ajax({
            url: "/api/guess/facilitiesProcessingCommodity/" + encodeURIComponent(this.state.selectedCommodityId),
            success: function(data){
                self.setState({
                    results: data,
                    processing: false
                });
            }
        });
    }
});