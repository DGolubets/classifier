var Graph = React.createClass({
    componentDidMount: function(){
        var container = this.getDOMNode();

        this.graph = new VisJsGraph(container);
        this.graph.render(this.props);
    },
    componentWillUnmount: function(){
        this.graph.destroy();
    },
    componentWillReceiveProps: function (nextProps){
        if(nextProps.categories != this.props.categories) {
            this.graph.render(nextProps);
        }
    },
    shouldComponentUpdate: function() {
        return false;
    },
    render: function(){
        return <div className="graph"></div>;
    }
});

/**
 * Library-specific graph code.
 * @param container DOM element where to draw it
 */
function VisJsGraph(container){

    // VisJs graph
    var network;

    // VisJs graph options
    var options = {
        layout: {
            hierarchical: {
                direction: 'UD',
                sortMethod: 'directed',
                levelSeparation: 205
            }
        },
        groups: {
            facility: { color:{ background:'#f00' } },
            vessel: { color:{ background:'#00f' }, font: { color: '#fff'}},
            cargo: { color:{ background:'#ccc' } },
            commodity: { color:{ background:'#0f0' } }
        },
        nodes: {
            shape: 'box'
        },
        //edges: {
        //    smooth: {
        //        type: 'cubicBezier',
        //        forceDirection: 'vertical'
        //    }
        //},
        physics: {
            enabled: true,
            hierarchicalRepulsion: {
                nodeDistance: 200,
                springConstant: 0.1
            }
        }
    };

    this.destroy = function() {
        if (network !== null) {
            network.destroy();
            network = null;
        }
    };

    this.render = function (props){

        // our graph is indeed a set of trees
        // so we can pre-arrange them
        // and that will help VisJs string-physics

        var trees = buildTrees(props.categories);

        trees.roots.forEach(function(tree, i){
            var width = getTreeWidth(tree);
            tree.width = width;
            var xMin = i * width;
            var xMax = xMin + width;

            var y = 0;
            switch(tree.category.tag){
                case CategoryTags.facility:
                    y = 0;
                    break;
                case CategoryTags.commodity:
                    y = 3;
                    break;
                case CategoryTags.vessel:
                    y = 6;
                    break;
                case CategoryTags.cargo:
                    y = 7;
                    break;
            }

            arrangeTree(tree, xMin, xMax, y);
        });


        // populate VisJs structurs

        var nodes = [];
        var edges = [];

        trees.nodes.forEach(function(tree) {
            var c = tree.category;
            nodes.push({
                id: c.id,
                label: c.name,
                group: c.tag,
                level: tree.y,
                x: tree.x
            });
        });

        props.categories.forEach(function(c) {
            // parent relations
            if(c.parentId){
                edges.push({ from: c.id, to: c.parentId, arrows: 'to' });
            }

            // other general relations
            if(c.relations) {
                c.relations.forEach(function (rid) {
                    edges.push({ from: c.id, to: rid, arrows: 'to', dashes: true, physics: false});
                });
            }
        });

        var data = {
            nodes: nodes,
            edges: edges
        };

        network = new vis.Network(container, data, options);

        if(props.selectedCategory){
            network.selectNodes([props.selectedCategory.id]);
        }

        network.on("select", function (params) {
            if(params.nodes.length){
                props.onSelect(trees.nodes[params.nodes[0]].category);
            }
            else{
                props.onSelect(null);
            }
        });
    };



    /**
     * Builds categories trees.
     * Returns both all tree nodes and tree roots.
     * @param categories
     */
    function buildTrees(categories){
        var roots = [];
        var trees = [];

        function ensureTree(id){
            var tree = trees[id];
            if(!tree){
                tree = { id: id, parent: null, children: [] };
                trees.push(tree);
                trees[id] = tree;
            }
            return tree;
        }

        categories.forEach(function(c) {
            var tree = ensureTree(c.id);
            if(!c.parentId){
                roots.push(tree);
            }
            else {
                tree.parent = ensureTree(c.parentId);
                tree.parent.children.push(tree);
            }
            tree.category = c;
        });

        return {
            nodes: trees,
            roots: roots
        };
    }

    /**
     * Calculates tree width as the number of leafs
     * @param tree
     * @returns {number}
     */
    function getTreeWidth(tree){
        var width = 0;
        for(var i = 0; i < tree.children.length; i++){
            width += getTreeWidth(tree.children[i]);
        }
        return width || 1;
    }

    /**
     * Arranges a tree in pseudo coordinates.
     * @param tree
     * @param xMin
     * @param xMax
     * @param y
     */
    function arrangeTree(tree, xMin, xMax, y){
        tree.x = xMin + (xMax - xMin) / 2;
        tree.y = y;

        if(tree.children.length) {
            var sWidth = (xMax - xMin) / tree.children.length;
            tree.children.forEach(function (subTree, i) {
                var sxMin = xMin + i * sWidth;
                var sxMax = sxMin + sWidth;
                arrangeTree(subTree, sxMin, sxMax, y + 1)
            });
        }
    }
}