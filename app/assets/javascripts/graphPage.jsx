var GraphPage = React.createClass({
    handleCreate: function(category){
        var self = this;
        $.ajax({
            method: 'POST',
            url: '/api/categories',
            data: JSON.stringify(category),
            contentType: 'application/json',
            success: function(data){
                category.id = data.id;
                var newCategories = React.addons.update(self.state.categories, {$push: [category]});
                self.setState({
                    categories: newCategories
                });
            }
        });
    },
    handleUpdate: function(category){
        var self = this;
        $.ajax({
            method: 'POST',
            url: '/api/categories',
            data: JSON.stringify(category),
            contentType: 'application/json',
            success: function(){
                var newCategories = self.state.categories.map(function(c){
                    return c.id == category.id ? category : c;
                });
                self.setState({
                    categories: newCategories
                });
            }
        });
    },
    handleRemove: function(category){
        var self = this;
        $.ajax({
            method: 'DELETE',
            url: '/api/categories/' + encodeURIComponent(category.id),
            success: function(){
                var newCategories = self.state.categories.filter(function(c){
                    return c.id != category.id;
                });
                newCategories.forEach(function(c){
                    if(c.parentId == category.id){
                        c.parentId = null;
                    }
                    var relIndex = c.relations.indexOf(category.id);
                    if(relIndex >= 0){
                        c.relations.splice(relIndex, 1);
                    }
                });
                self.setState({
                    categories: newCategories
                });
            }
        });
    },
    handleSelect: function(category){
        this.setState({
            selectedCategoryId: category ? category.id : null
        });
    },
    componentDidMount: function(){
        var component = this;
        $.ajax({
            url: "/api/categories",
            success: function(data){
                component.setState({
                    categories: data
                })
            }
        });
    },
    getInitialState: function(){
      return {
          categories: [],
          selectedCategoryId: null
      };
    },
    render: function(){
        var self = this;
        var selectedCategory = this.state.categories.find(function(c){
           return c.id == self.state.selectedCategoryId;
        });
        return (
            <div className="page">
                <Toolbar categories={this.state.categories} selectedCategory={selectedCategory}
                         onCreate={this.handleCreate} onRemove={this.handleRemove} onUpdate={this.handleUpdate} />
                <Graph categories={this.state.categories} selectedCategory={selectedCategory} onSelect={this.handleSelect} />
            </div>
        );
    }
});