/**
 * Root project.
 */
lazy val root = Project("classifier", file("."))
				.enablePlugins(PlayScala)
				.settings(commonSettings ++ dataSettings ++ webSettings)


/**
 * Common settings and dependencies.
 */
lazy val commonSettings = Defaults.coreDefaultSettings ++ Seq(
  organization := "com.commodityvectors",
  version := "0.1.0",

  scalaVersion := "2.11.7",
  version := "1.0.0",

  resolvers += "Typesafe repository" at "https://repo.typesafe.com/typesafe/releases/",
  libraryDependencies ++= Seq(
 
    // runtime
    "com.typesafe" % "config" % "1.2.1", // config
    "ch.qos.logback" % "logback-classic" % "1.1.1" % "compile", // logger
    "com.typesafe.scala-logging" %% "scala-logging" % "3.1.0", //  logging facade,
    "org.scala-lang.modules" %% "scala-async" % "0.9.5", // async flow

    // test
    "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test",
    "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % "test"
  )
)

/**
 * Data settings and dependencies.
 */
lazy val dataSettings = commonSettings ++ Seq(
  libraryDependencies ++= Seq(
    "org.reactivemongo" %% "reactivemongo" % "0.11.6"
  )
)

/**
 * Web settings and dependencies.
 */
lazy val webSettings = commonSettings ++ Seq(
  includeFilter in (Assets, LessKeys.less) := "*.less",
  routesGenerator := InjectedRoutesGenerator,

  libraryDependencies ++= Seq(
    "org.webjars" % "bootstrap" % "3.3.4"
  )
)